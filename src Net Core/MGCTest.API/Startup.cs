using Autofac;
using MGCTest.Infrastructure.Bootstrap;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System.Globalization;

namespace MGCTest.API
{
    public class Startup
    {
        private readonly ApplicationStartup applicationStartup;

        public Startup(IConfiguration configuration)
        {
            this.applicationStartup = new ApplicationStartup(configuration);
        }

        public void ConfigureServices(IServiceCollection services)
        {
            this.applicationStartup.ConfigureServices(services);
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IApiVersionDescriptionProvider provider)
        {
            this.applicationStartup.Configure(app, provider);
            if (env.EnvironmentName == Environments.Development)
            {
                app.UseDeveloperExceptionPage();
            }
        }

        public void ConfigureContainer(ContainerBuilder builder)
        {
            this.applicationStartup.ConfigureContainer(builder);
        }

    }
}
