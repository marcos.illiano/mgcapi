using MGCTest.Domain.Domain;
using MGCTest.Domain.Dtos;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace MGCTest.API.Controllers.V1.Branches
{
    /// <summary>
    /// </summary>
    [ApiVersion("1.0")]
    [Route("v{v:apiVersion}")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {

        private readonly IEmployeeService service;

        public EmployeeController(IEmployeeService service)
        {
            this.service = service;
        }


        /// <summary>
        /// Api to return information for a given employee or multiple employees
        /// </summary>
        /// <returns></returns>
        /// <param name="employeeId">Identifier Id for employee. Leave empty value to return all employees</param>
        [HttpGet]
        [Route("Employee")]
        [ProducesResponseType(typeof(List<EmployeeDto>), 200)]
        public ActionResult<object> GetEmployees(int? employeeId)
        {
            return this.service.GetEmployees(employeeId);
        }



    }
}

