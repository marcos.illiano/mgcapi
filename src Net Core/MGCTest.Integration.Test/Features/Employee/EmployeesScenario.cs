using System.Threading.Tasks;
using MGCTest.Integration.Test.Setup;
using FakeItEasy;
using MGCTest.Domain.Repository;
using MGCTest.Domain.Domain;
using MGCTest.Domain.Dtos;
using Xunit;
using System.Collections.Generic;

namespace MGCTest.Integration.Test.Features.Branches
{
    public class EmployeesScenario : ScenarioBase
    {
        //Test to validate the calculation of Anual Salary
        [Fact]
        public async Task GetAnualSalary()
        {
            var repository = A.Fake<IEmployeeRepository>();
            var service = new EmployeeService(repository);

            A.CallTo(() => repository.GetEmployees()).Returns(this.responseFake);

            var result = service.GetEmployeeById(1);

            Assert.NotNull(result);
            Assert.NotNull(result.AnualSalary);

            Assert.Equal(12000, result.AnualSalary);

        }


        private readonly List<EmployeeDto> responseFake = new List<EmployeeDto>() { new EmployeeDto
        {
            Id = 1,
            ContractTypeName = "MonthlySalaryEmployee",
            HourlySalary = 20,
            MonthlySalary = 1000,
            Name = "Marcos",
            RoleDescription = "",
            RoleId = 1
        } };
    }
}
