﻿using MGCTest.API;
using Microsoft.Extensions.Configuration;

namespace MGCTest.Integration.Test.Setup
{
    public class TestsStartUp : Startup
    {

        public TestsStartUp(IConfiguration configuration) : base(configuration)
        {

        }

    }
}
