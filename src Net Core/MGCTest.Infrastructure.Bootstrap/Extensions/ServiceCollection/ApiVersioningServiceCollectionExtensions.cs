using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;

namespace MGCTest.Infrastructure.Bootstrap.Extensions.ServiceCollection
{
    public static class ApiVersioningServiceCollectionExtensions
    {
        public static void AddApiVersion(this IServiceCollection services, string defaultVersion)
        {
            services.AddVersionedApiExplorer(o =>
            {
                o.GroupNameFormat = "'v'VVV";
                o.SubstituteApiVersionInUrl = true;
            });

            services.AddApiVersioning(o =>
            {
                o.ReportApiVersions = true;
                o.AssumeDefaultVersionWhenUnspecified = true;                
                o.DefaultApiVersion = ApiVersion.Parse(defaultVersion);
            });
        }
    }
}
