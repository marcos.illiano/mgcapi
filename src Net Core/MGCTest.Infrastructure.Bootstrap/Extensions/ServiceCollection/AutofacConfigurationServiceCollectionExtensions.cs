using Autofac;
using MGCTest.Infrastructure.Bootstrap.AutofacModules;
using MGCTest.Infrastructure.Bootstrap.AutofacModules.Features;
using Microsoft.Extensions.Configuration;

namespace MGCTest.Infrastructure.Bootstrap.Extensions.ServiceCollection
{
    public static class AutofacConfigurationServiceCollectionExtensions
    {
        public static void AddConfigurationAutofac(this ContainerBuilder builder, IConfiguration configuration)
        {            
            builder.RegisterModule<EmployeeModule>();               
        }
    }
}
