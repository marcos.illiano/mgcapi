using System;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace MGCTest.Infrastructure.Bootstrap.Extensions.ServiceCollection
{
    public static class SwaggerServiceCollectionExtensions
    {
        /// <summary>  
        /// Represents the Swagger/Swashbuckle operation filter used to document the implicit API version parameter.  
        /// </summary>  
        /// <remarks>This <see cref="IOperationFilter"/> is only required due to bugs in the <see cref="SwaggerGenerator"/>.  
        /// Once they are fixed and published, this class can be removed.</remarks>
        private class SwaggerDefaultValues : IOperationFilter
        {
            /// <summary>  
            /// Applies the filter to the specified operation using the given context.  
            /// </summary>  
            /// <param name="operation">The operation to apply the filter to.</param>  
            /// <param name="context">The current operation filter context.</param>  
            public void Apply(OpenApiOperation operation, OperationFilterContext context)
            {
                if (operation.Parameters == null)
                {
                    return;
                }

                foreach (var parameter in operation.Parameters)
                {
                    //Read the parameters metadata from the AspNetCore.Servicing.Explorer and copy the default value to the OpenApi spec
                    //(i.e fill api-version according the selected swagger document)
                    var description = context.ApiDescription.ParameterDescriptions.FirstOrDefault(p => p.Name == parameter.Name);

                    if (parameter.Schema.Default == null && description?.DefaultValue != null)
                    {
                        parameter.Schema.Default = OpenApiAnyFactory.CreateFor(parameter.Schema, description.DefaultValue);
                    }
                }
            }
        }

        private class AddRequiredHeaderParameter : IOperationFilter
        {
            /// <summary>  
            /// Applies the filter to the specified operation using the given context.  
            /// </summary>  
            /// <param name="operation">The operation to apply the filter to.</param>  
            /// <param name="context">The current operation filter context.</param>  
            public void Apply(OpenApiOperation operation, OperationFilterContext context)
            {
                if (operation.Parameters == null)
                {
                    return;
                }

                operation.Parameters.Add(new OpenApiParameter
                {
                    Name = "Api-Key",
                    In = ParameterLocation.Header,
                    Required = false
                });
            }
        }


        public static void AddSwagger(this IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                var provider = services.BuildServiceProvider().GetRequiredService<IApiVersionDescriptionProvider>();

                foreach (var description in provider.ApiVersionDescriptions)
                {
                    c.SwaggerDoc(description.GroupName, CreateInfoForApiVersion(description));
                }

                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Description = @"JWT Authorization header using the Bearer scheme. \r\n\r\n 
                      Enter 'Bearer' [space] and then your token in the text input below.
                      \r\n\r\nExample: 'Bearer 12345abcdef'",
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer"
                });
                c.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    { new OpenApiSecurityScheme { Name = "Bearer" }, new List<string>() },
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            },
                            Name = "Bearer",
                            In = ParameterLocation.Header
                        }
                        , new List<string>()
                    }
                });

                // Add a custom filter for settint the default values  
                c.OperationFilter<SwaggerDefaultValues>();

                c.OperationFilter<AddRequiredHeaderParameter>();

                // Set the comments path for the Swagger JSON and UI.
                var xmlFile = $"{GetApplicationName()}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                if (File.Exists(xmlPath))
                {
                    c.IncludeXmlComments(xmlPath);
                }
            });
        }

        private static OpenApiInfo CreateInfoForApiVersion(ApiVersionDescription description)
        {
            var info = new OpenApiInfo
            {
                Title = GetApplicationName(),
                Version = description.ApiVersion.ToString()
            };

            info.Description += @"API to obtain Employees Information <br />";

            return info;
        }

        private static string GetApplicationName()
        {
            return (System.Reflection.Assembly.GetEntryAssembly() ?? System.Reflection.Assembly.GetExecutingAssembly()).GetName().Name;
        }
    }
}
