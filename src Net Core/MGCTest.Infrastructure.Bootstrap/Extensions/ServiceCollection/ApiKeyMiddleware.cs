using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MGCTest.Infrastructure.Bootstrap.Extensions.ServiceCollection
{
    public class ApiKeyMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly IConfiguration configuration;
        private const string APIKEYNAME = "Api-Key";
        public ApiKeyMiddleware(RequestDelegate next, IConfiguration configuration)
        {
            _next = next;
            this.configuration = configuration;
        }
        public async Task InvokeAsync(HttpContext context)
        {

            if (configuration["AppSettings:IncludeApiKeyAuthentication"] == "true")
            {

                if (!context.Request.Headers.TryGetValue(APIKEYNAME, out var extractedApiKey))
                {
                    context.Response.StatusCode = 401;
                    await context.Response.WriteAsync("Api Key was not provided. (Using ApiKeyMiddleware) ");
                    return;
                }

                var apiKey = configuration["AppSettings:ApiKey"];

                if (!apiKey.Equals(extractedApiKey))
                {
                    context.Response.StatusCode = 401;
                    await context.Response.WriteAsync("Unauthorized client. (Using ApiKeyMiddleware)");
                    return;
                }
            }

            await _next(context);
        }
    }
}
