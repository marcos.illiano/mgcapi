using Autofac;
using MGCTest.Infrastructure.Bootstrap.Extensions.ApplicationBuilder;
using MGCTest.Infrastructure.Bootstrap.Extensions.ServiceCollection;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Globalization;

namespace MGCTest.Infrastructure.Bootstrap
{
    public class ApplicationStartup
    {
        private readonly IConfiguration configuration;     

        public ApplicationStartup(IConfiguration configuration)
        {
            this.configuration = configuration;            
        }        

        public void ConfigureServices(IServiceCollection services)
        {

            services.AddMicroserviceExampleHealthChecks();
            services.AddSwagger();
            services.AddHttpContextAccessor();            
            services.AddCorsConfiguration();            
            services.AddApiVersion(this.configuration.GetValue<string>("AppSettings:DefaultApiVersion", "1.0"));
            services.AddControllers(o =>
            {
                o.Filters.Add(new ProducesResponseTypeAttribute(200));
                o.Filters.Add(new ProducesResponseTypeAttribute(400));
                o.Filters.Add(new ProducesResponseTypeAttribute(500));
            });
        }


        public void ConfigureContainer(ContainerBuilder builder)
        {
            builder.AddConfigurationAutofac(this.configuration);
        }

        public void Configure(IApplicationBuilder app, IApiVersionDescriptionProvider provider)
        {

            app.UseRouting();
            app.UseAuthentication();
            app.UseMicroserviceExampleHealthChecks();
            app.UseMicroserviceExampleSwagger(provider);
            app.UseCorsConfiguration();

            app.UseExceptionHandler(errorPipeline =>
            {
                errorPipeline.UseExceptionHandlerMiddleware();
            });

            app.UseMiddleware<ApiKeyMiddleware>();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapDefaultControllerRoute();
                endpoints.MapControllers();
            });

        }

    }
}
