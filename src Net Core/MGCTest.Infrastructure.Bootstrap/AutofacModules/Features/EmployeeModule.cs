using Autofac;
using MGCTest.Domain.Repository;
using MGCTest.Domain.Domain;

namespace MGCTest.Infrastructure.Bootstrap.AutofacModules.Features
{
    public class EmployeeModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<EmployeeService>()
             .As<IEmployeeService>()
             .InstancePerLifetimeScope();

            builder.RegisterType<EmployeeRepository>()
             .As<IEmployeeRepository>()
             .InstancePerLifetimeScope();

        }
    }
}
