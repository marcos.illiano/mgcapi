using MGCTest.Domain.Dtos;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;

namespace MGCTest.Domain.Repository
{
    public class EmployeeRepository :  IEmployeeRepository
    {
        private readonly IConfiguration configuration;

        public EmployeeRepository(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public List<EmployeeDto> GetEmployees()
        {

            using (var httpClient = new HttpClient())
            {

                var response = httpClient.GetAsync(this.configuration.GetSection("AppSettings:MGCEmployeUrl").Value).Result;
               
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var contents = response.Content.ReadAsStringAsync();

                    var responseDto = JsonConvert.DeserializeObject<List<EmployeeDto>>(contents.Result);

                    return responseDto;
                }

            }


            throw new Exception("Error calling MGCEmploye service");

        }


    }
}
