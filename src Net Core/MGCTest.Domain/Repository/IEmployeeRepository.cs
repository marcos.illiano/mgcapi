using System.Collections.Generic;
using MGCTest.Domain.Dtos;

namespace MGCTest.Domain.Repository
{
    public interface IEmployeeRepository
    {
        /// <returns><see cref="EmployeeDto"/></returns>
        List<EmployeeDto> GetEmployees();

    }
}
