﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MGCTest.Domain.Dtos
{
    public abstract class SalaryDto
    {
        public double HourlySalary { get; set; }

        public double MonthlySalary { get; set; }

        public double? AnualSalary { get; set; }

        public abstract void CalculateAnualSalary();
    }
}
