using MGCTest.Domain;
using System;

namespace MGCTest.Domain.Dtos
{
    public class EmployeeDto : SalaryDto
    {

        public int Id { get; set; }

        public string Name { get; set; }

        public string ContractTypeName { get; set; }

        public int RoleId { get; set; }

        public string RoleName { get; set; }

        public string RoleDescription { get; set; }

        public override void CalculateAnualSalary()
        {
            switch (ContractTypeName)
            {
                case Constants.HourlySalaryEmployee:
                    AnualSalary = 120 * HourlySalary * 12;
                    break;

                case Constants.MonthlySalaryEmployee:
                    AnualSalary = MonthlySalary * 12;
                    break;

                default:
                    AnualSalary = null;
                    break;
            }

        }

    }

}
