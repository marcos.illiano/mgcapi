﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MGCTest.Domain
{
    public class Constants
    {

        public const string HourlySalaryEmployee = "HourlySalaryEmployee";

        public const string MonthlySalaryEmployee = "MonthlySalaryEmployee";
    }
}
