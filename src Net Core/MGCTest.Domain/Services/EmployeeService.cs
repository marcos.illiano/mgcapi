using System;
using System.Collections.Generic;
using System.Linq;
using MGCTest.Domain.Dtos;
using MGCTest.Domain.Repository;

namespace MGCTest.Domain.Domain
{
    public class EmployeeService : IEmployeeService
    {
        private readonly IEmployeeRepository employeeRepository;

        public EmployeeService(IEmployeeRepository employeeRepository)
        {
            this.employeeRepository = employeeRepository;
        }

        public List<EmployeeDto> GetEmployees(int? employeeId)
        {
            var result = new List<EmployeeDto>();

            if (employeeId != null)
            {
                var employee = GetEmployeeById(employeeId.Value);

                if(employee != null)
                result.Add(employee);

                return result;

            }

            return GetAllEmployeesData();
        }

        public EmployeeDto GetEmployeeById(int id)
        {
            var employees = this.GetAllEmployeesData();

            return employees.FirstOrDefault(e => e.Id == id);

        }

        #region Private

        private List<EmployeeDto> GetAllEmployeesData()
        {
            var employees = employeeRepository.GetEmployees();

            if (employees == null )
            {
                return new List<EmployeeDto>();
            }

            employees.ForEach(c =>
            {
                c.CalculateAnualSalary();
            });

            return employees;
        }

        #endregion 
    }
}
