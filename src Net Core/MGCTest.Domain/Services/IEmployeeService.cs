using System.Collections.Generic;
using MGCTest.Domain.Dtos;

namespace MGCTest.Domain.Domain
{
    /// <summary>
    /// Defines methods to magane the employees.
    /// </summary>
    public interface IEmployeeService
    {
        List<EmployeeDto> GetEmployees(int? employeeId);

        EmployeeDto GetEmployeeById(int id);

    }
}
