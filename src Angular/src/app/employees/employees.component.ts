import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';
import { Employee } from '../models/employee';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.scss']
})
export class EmployeeComponent implements OnInit {

  displayedColumns: string[] = ['id', 'name','contractTypeName', 'roleId','roleName', 'roleDescription', 'hourlySalary', 'monthlySalary', 'anualSalary'];
  data: Employee[] = [];
  isLoadingResults = true;

  constructor(private api: ApiService, private router: Router) { }

  ngOnInit() {
  }

  getEmployees(id: number)
  {

    if(id)
    {
      this.api.getEmployeeById(id)
      .subscribe((res: any) => {
        this.data = res;
        console.log(this.data);
        this.isLoadingResults = false;
      }, err => {
        console.log(err);
        this.isLoadingResults = false;
      });

    }
    else{

      this.api.getEmployees()
      .subscribe((res: any) => {
        this.data = res;
        console.log(this.data);
        this.isLoadingResults = false;
      }, err => {
        console.log(err);
        this.isLoadingResults = false;
      });

    }
   

  }

  


}
